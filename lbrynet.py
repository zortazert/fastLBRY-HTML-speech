"""
Run this script to start up lbrynet. Change the value of the lbrynet
binary so it works on your system. You need to give it the EXACT path
to the file at less you put it in your system's path then you can just
write lbrynet. Pressing Control + c or just quiting out of the terminal should stop the lbrynet process.
"""

import subprocess

default_settings = {
    "lbrynet_binary": "C:/SGZ_Pro/Hobbys/coding-projects/Python/libbry-master/lbrynet.exe"
}

subprocess.Popen([default_settings["lbrynet_binary"], "start"])
