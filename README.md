# FastLBRY-HTML-speech

This is a fork of the HTML client for LBRY ( Part of FastLBRY project ) AGPL that relys on spee.ch for getting the MD from articles on LBRY rather than the SDK.

The advantage of this is that it's something to use when the SDK for downloading articles isn't working on your system.

Original Project that uses the SDK:
https://notabug.org/jyamihud/FastLBRY-HTML
